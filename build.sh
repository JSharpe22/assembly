#!/bin/bash

protoc -I ./api/v1/src ./api/v1/src/* --go_out=plugins=grpc:api/v1

cd transport
go build -o ../bin/transport *.go

cd ../brain
go build -o ../bin/brain *.go
