#!/bin/bash

CMD1="./bin/transport"
CMD2="./bin/brain"
# echo 'kill %1; kill %2'
# trap 'kill %1; kill %2' SIGINT
# $CMD1 & 
$CMD2 &

trap "pkill -P $$" SIGINT
wait

# trap "echo Booh!" SIGINT SIGTERM
# echo "it's going to run until you hit Ctrl+Z"
# echo "hit Ctrl+C to be blown away!"

# while :         
# do
#     sleep 60       
# done
