package brain

import (
	"context"
	"log"

	api "leolaunches.com/assembly/api/v1"
	"leolaunches.com/assembly/brick"
	"leolaunches.com/assembly/physics"
)

type State string

const (
	placingBrick   State = "placing_brick"
	goingHome      State = "going_home"
	pickingUpBrick State = "picking_up_brick"
	waiting        State = "waiting"
)

type System interface {
	SetGoal(context.Context) (api.TransportService_SetGoalClient, error)
	GetBrickAndSendGoal(context.Context, api.TransportService_SetGoalClient)
	NextGoal(context.Context, api.TransportService_SetGoalClient, chan bool)
}

// Brain stands for (Br)ick (A)rm and (In)structor
// This object will be in charge of talking to transport system
// to instruct it where it needs to be moved to place the current brick.
// Once placed, the brain will upload a configuration to the brick and lock
// the brick once connected correctly.
type Brain struct {
	currentConfig *brick.Brick `json:"current_config"`
	state         State
	arm           *Arm

	transportation TransportationClient
	attachedBrick  BrickClient `json:"attached_brick"`

	mission *Mission `json:"mission"`

	*physics.Kinematic `json:"kinematic"`
	Env                *physics.Environment
}

func RunMission(s System) {
	stream, err := s.SetGoal(context.Background())
	if err != nil {
		log.Fatalf("open stream error %v", err)
	}

	ctx := stream.Context()
	done := make(chan bool)

	go s.GetBrickAndSendGoal(ctx, stream)
	go s.NextGoal(ctx, stream, done)
	<-done
}

func New() *Brain {
	// Eventuially need to load arm.
	return &Brain{
		state:          waiting,
		transportation: &transportation{},
		attachedBrick:  &brickClient{},
		Kinematic:      physics.NewKinematic(0),
	}
}

func (b *Brain) ConnectToTransport() {
	b.transportation.Connect()
}

func (b *Brain) ConnectToBrick(id string) {
	b.attachedBrick.Connect(id)
}

func (b *Brain) LoadMission(m *Mission) {
	b.mission = m
}

func (b *Brain) SetGoal(ctx context.Context) (api.TransportService_SetGoalClient, error) {
	return b.transportation.SetGoal(ctx)
}

func (b *Brain) getBrick(ctx context.Context) *brick.Brick {
	newBrick, err := b.transportation.ReleaseBrick(ctx, &api.ReleaseBrickCommand{})
	if err != nil {
		log.Fatalf("Unable to release brick. %v", err)
	}

	return b.mission.PopBrick(newBrick.Id)
}

func (b *Brain) updateBrainWithNewBrick(newBrick *brick.Brick) {
	b.ConnectToBrick(newBrick.ID)
	b.currentConfig = newBrick
}

func (b *Brain) sendNewLocationGoal(stream api.TransportService_SetGoalClient, newLocation *physics.Vector) {
	goal := newGoal(newLocation, newLocation.CompareVector(physics.NewVector(0, 0, 0)))

	err := stream.Send(goal)
	if err != nil {
		log.Fatalf("Unable to set new goal. %v", err)
	}
}

func (b *Brain) GetBrickAndSendGoal(ctx context.Context, stream api.TransportService_SetGoalClient) {
	newBrick := b.getBrick(ctx)
	b.updateBrainWithNewBrick(newBrick)
	b.sendNewLocationGoal(stream, newBrick.LocationGoal)
}

func (b *Brain) uploadConfiguration(ctx context.Context) {
	config, err := brick.GetGRPCConfiguration(b.currentConfig)
	if err != nil {
		log.Fatalf("Unable to convert configuration to grpc object. %v", err)
	}

	// Set connections/routing
	status, err := b.attachedBrick.UploadConfiguration(ctx, config)
	if err != nil || !status.Success {
		log.Fatalf("Configuration Upload Failed. If error is nil, reason is unknown. Err=%v", err)
	}
}

func (b *Brain) makeConnections(ctx context.Context, newBrick *brick.Brick) {
	for currentLocation, brickToCheck := range b.currentConfig.ConnectedBricks {
		// Check if brick that new brick is being connected to has been placed
		if placedBrick, doesExist := b.mission.PlacedBricks[brickToCheck.BrickID]; doesExist {
			// Connect two bricks.
			connection := &brick.Connection{
				BrickSrc:                 newBrick,
				BrickToConnect:           placedBrick,
				ConnectionInfo:           brickToCheck,
				LocationBrickSrc:         currentLocation,
				LocationOnBrickToConnect: brickToCheck.LocationConnectedTo,
			}
			connGRPC, err := brick.GetGRPCConnectionConfiguration(connection)
			if err != nil {
				log.Fatalf("Unable to convert connection to grpc object. %v", err)
			}

			b.attachedBrick.ConnectBrick(ctx, connGRPC) // Change to locks?
		}
	}
}

func (b *Brain) configureBrick(ctx context.Context) {
	b.uploadConfiguration(ctx)
	b.makeConnections(ctx, b.attachedBrick.GetBrick())
	b.removeBrick()
}

// TODO Add tests for each and method and break up below here.
func (b *Brain) goHome(ctx context.Context, stream api.TransportService_SetGoalClient) {
	goal := newGoal(physics.NewVector(0, 0, 0), true)

	err := stream.Send(goal)
	if err != nil {
		log.Fatalf("Unable to set new goal. %v", err)
	}
	b.state = goingHome
}

func (b *Brain) NextGoal(ctx context.Context, stream api.TransportService_SetGoalClient, done chan bool) {
	for {
		// Recieve next goal
		resp, err := stream.Recv()
		if err != nil {
			log.Fatalf("Unable to retrieve goal mission. %v", err)
		}
		if !resp.Success {
			log.Fatalf("Mission failed. Problem reaching goal location.")
		}

		// Check if the mission is complete. if so, return.
		if b.mission.IsComplete {
			err = stream.CloseSend()
			if err != nil {
				log.Fatalf("Close connection failed. %v", err)
			}

			log.Println("Mission Results: " + b.mission.String())

			b.Env.MarkDone()
			close(done)
			return
		}

		// If brick is not attached, then home and need to get brick.
		if b.attachedBrick == nil {
			b.GetBrickAndSendGoal(ctx, stream)
			continue
		}

		// If brick is attached, then neeed to configure brick .
		b.configureBrick(ctx)
		b.goHome(ctx, stream)
	}
}

func (b *Brain) removeBrick() {
	b.mission.AddPlacedBrick(b.attachedBrick.GetBrick())
	b.currentConfig = nil

	b.attachedBrick.Close()
	b.attachedBrick = nil
}

func (b *Brain) Update(dt float64) {
	if b.state == waiting {
		RunMission(b)
	}
	b.Kinematic.Update(dt)
}
