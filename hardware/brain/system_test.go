package brain

import (
	"context"
	"testing"

	"leolaunches.com/assembly/physics"

	"leolaunches.com/assembly/brick"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	api "leolaunches.com/assembly/api/v1"
)

/*
 * Definitions
 */

const (
	releasedBrickID = "5394"
)

// Mock Transport Client
type fakeTransportSystem struct {
	didRunConnect   bool
	didRunClose     bool
	didSetGoal      bool
	didReleaseBrick bool
}

func (t *fakeTransportSystem) Connect() TransportationClient {
	t.didRunConnect = true
	return t
}

func (t *fakeTransportSystem) Close() {
	t.didRunClose = true
}

func (t *fakeTransportSystem) SetGoal(ctx context.Context, opts ...grpc.CallOption) (api.TransportService_SetGoalClient, error) {
	t.didSetGoal = true
	return nil, nil
}

func (t *fakeTransportSystem) ReleaseBrick(ctx context.Context, in *api.ReleaseBrickCommand, opts ...grpc.CallOption) (*api.BrickReleased, error) {
	t.didReleaseBrick = true
	return &api.BrickReleased{
		Id: releasedBrickID,
	}, nil
}

// Mock Brick Client
type fakeBrickClient struct {
	didRunConnect          bool
	didRunClose            bool
	didGetBrick            bool
	didUploadConfiguration bool
	didConnectBrick        bool
}

func (b *fakeBrickClient) Connect(id string) BrickClient {
	b.didRunConnect = true
	return b
}

func (b *fakeBrickClient) Close() {
	b.didRunClose = true
}

func (b *fakeBrickClient) GetBrick() *brick.Brick {
	return &brick.Brick{}
}

func (b *fakeBrickClient) UploadConfiguration(ctx context.Context, in *api.Configuration, opts ...grpc.CallOption) (*api.UploadStatus, error) {
	b.didUploadConfiguration = true
	return &api.UploadStatus{Success: true}, nil
}

func (b *fakeBrickClient) ConnectBrick(ctx context.Context, in *api.ConnectionConfiguration, opts ...grpc.CallOption) (*api.ConnectionStatus, error) {
	b.didConnectBrick = true
	return nil, nil
}

// Mission
func getBaseMission() *Mission {
	return &Mission{
		Environment:      "orbital",
		DeliveryCapacity: 15,
		BricksToPlace: map[string]*brick.Brick{
			releasedBrickID: getReleasedBrick(),
			"5395":          getReleasedBrick5395(),
		},
	}
}

// Bricks
func getReleasedBrick() *brick.Brick {
	return &brick.Brick{
		ID:           releasedBrickID,
		LocationGoal: physics.NewVector(5, 12.2, 13),
		ConnectedBricks: map[brick.SideLocation]*brick.ConnectedBrick{
			"0": &brick.ConnectedBrick{
				BrickID:             "5395",
				LocationConnectedTo: "4",
				AngleConnected:      0,
			},
		},
		Connections: map[brick.SideLocation]map[brick.ConnType][]string{
			"0": {
				"Electric": []string{
					"4",
				},
			},
		},
	}
}

func getReleasedBrick5395() *brick.Brick {
	return &brick.Brick{
		ID:           "5395",
		LocationGoal: physics.NewVector(5, 12.2, 19),
		ConnectedBricks: map[brick.SideLocation]*brick.ConnectedBrick{
			"4": &brick.ConnectedBrick{
				BrickID:             releasedBrickID,
				LocationConnectedTo: "0",
				AngleConnected:      0,
			},
		},
		Connections: map[brick.SideLocation]map[brick.ConnType][]string{
			"4": {
				"Electric": []string{
					"0",
				},
			},
		},
	}
}

// Stream
type mockTransportServiceSetGoalClient struct {
	didRunSend bool
	sendValue  *api.Goal
	didRunRecv bool
}

func (m *mockTransportServiceSetGoalClient) Send(goal *api.Goal) error {
	m.didRunSend = true
	m.sendValue = goal
	return nil
}

func (m *mockTransportServiceSetGoalClient) Recv() (*api.GoalResults, error) {
	return nil, nil
}

func (m *mockTransportServiceSetGoalClient) Header() (metadata.MD, error) {
	return nil, nil
}
func (m *mockTransportServiceSetGoalClient) Trailer() metadata.MD {
	return nil
}
func (m *mockTransportServiceSetGoalClient) CloseSend() error {
	return nil
}
func (m *mockTransportServiceSetGoalClient) Context() context.Context {
	return nil
}
func (m *mockTransportServiceSetGoalClient) SendMsg(mBlank interface{}) error {
	return nil
}
func (m *mockTransportServiceSetGoalClient) RecvMsg(mBlank interface{}) error {
	return nil
}

/*
 * Tests
 */
func TestNew(t *testing.T) {
	b := New()

	if b == nil {
		t.Fatal("New brain not created.")
	}
}

func TestConnectToTransport(t *testing.T) {
	b := &Brain{}

	tran := &fakeTransportSystem{}
	b.transportation = tran

	if tran.didRunConnect {
		t.Fatal("transportation.didRunConnect is true. Test will be invalid.")
	}

	b.ConnectToTransport()

	if !tran.didRunConnect {
		t.Fatal("transportation.didRunConnect is not true.")
	}
}

func TestConnectToBrick(t *testing.T) {
	b := &Brain{}

	brick := &fakeBrickClient{}
	b.attachedBrick = brick

	if brick.didRunConnect {
		t.Fatal("brick.didRunConnect is true. Test will be invalid.")
	}

	b.ConnectToBrick("test")

	if !brick.didRunConnect {
		t.Fatal("brick.didRunConnect is not true.")
	}
}

func TestSetGoal(t *testing.T) {
	b := &Brain{}

	tran := &fakeTransportSystem{}
	b.transportation = tran

	if tran.didSetGoal {
		t.Fatal("transportation.didRunConnect is true. Test will be invalid.")
	}

	b.SetGoal(context.Background())

	if !tran.didSetGoal {
		t.Fatal("transportation.didRunConnect is not true.")
	}
}

func TestLoadMission(t *testing.T) {
	size := 10
	m := Mission{
		DeliveryCapacity: size,
	}
	m.DeliveryCapacity = size
	b := &Brain{}

	if b.mission != nil {
		t.Fatal("Mission is not null. Test will be invalid.")
	}

	b.LoadMission(&m)

	if b.mission == nil {
		t.Fatal("Mission is null. Mission not loaded correctly.")
	}

	if b.mission.DeliveryCapacity != size {
		t.Fatal("Mission capacity is not set correctly. Mission not loaded correctly.")
	}
}

func TestGetBrick(t *testing.T) {
	mockBrick := getReleasedBrick()
	b := &Brain{
		transportation: &fakeTransportSystem{},
		mission:        getBaseMission(),
	}

	newBrick := b.getBrick(context.Background())

	if newBrick == nil {
		t.Fatal("Brick to be released is nil. GetBrick was not correct.")
	}

	if newBrick.ID != releasedBrickID {
		t.Fatal("Brick ids do not match. GetBrick was not correct.")
	}

	if !newBrick.LocationGoal.CompareVector(mockBrick.LocationGoal) {
		t.Fatal("Brick location vectors do not match. GetBrick was not correct.")
	}
}

func TestUpdatedBrainWithNewBrick(t *testing.T) {
	brickClient := &fakeBrickClient{}

	b := &Brain{
		attachedBrick: brickClient,
	}

	b.updateBrainWithNewBrick(getReleasedBrick())

	if !brickClient.didRunConnect {
		t.Fatal("Brick client not connected.")
	}

	if b.currentConfig == nil {
		t.Fatal("currentConfig not set. ")
	}

	if b.currentConfig.ID != releasedBrickID {
		t.Fatal("Current config not initialized with id.")
	}
}

func TestSendNewLocationGoal(t *testing.T) {
	streamTesting := &mockTransportServiceSetGoalClient{}

	b := &Brain{}

	currentVector := physics.NewVector(0, 0, 0)
	expectedReturnToBase := true

	b.sendNewLocationGoal(streamTesting, currentVector)

	if !streamTesting.didRunSend {
		t.Fatal("Did not run send when setting new goal.")
	}

	if streamTesting.sendValue.GetLocation().X != currentVector.X ||
		streamTesting.sendValue.GetLocation().Y != currentVector.Y ||
		streamTesting.sendValue.GetLocation().Z != currentVector.Z {

		t.Fatal("Invalid vector value set for new goal.")
	}

	if streamTesting.sendValue.GetReturnToBase() != expectedReturnToBase {
		t.Fatal("Invalid return to base set for new goal.")
	}

	streamTesting.didRunSend = false
	currentVector = physics.NewVector(1, 1, 1)
	expectedReturnToBase = false

	b.sendNewLocationGoal(streamTesting, currentVector)

	if !streamTesting.didRunSend {
		t.Fatal("Did not run send when setting new goal.")
	}

	if streamTesting.sendValue.GetLocation().X != currentVector.X ||
		streamTesting.sendValue.GetLocation().Y != currentVector.Y ||
		streamTesting.sendValue.GetLocation().Z != currentVector.Z {

		t.Fatal("Invalid vector value set for new goal.")
	}

	if streamTesting.sendValue.GetReturnToBase() != expectedReturnToBase {
		t.Fatal("Invalid return to base set for new goal.")
	}

}

func TestGetBrickAndSendGoal(t *testing.T) {
	newBrick := getReleasedBrick()
	mockMission := getBaseMission()
	expectedReturnToBase := false
	expectedVector := newBrick.LocationGoal

	brickClient := &fakeBrickClient{}

	b := &Brain{
		transportation: &fakeTransportSystem{},
		mission:        mockMission,
		attachedBrick:  brickClient,
	}

	streamTesting := &mockTransportServiceSetGoalClient{}

	b.GetBrickAndSendGoal(context.Background(), streamTesting)

	if !brickClient.didRunConnect {
		t.Fatal("Brick client not connected.")
	}

	if b.currentConfig == nil {
		t.Fatal("currentConfig not set. ")
	}

	if b.currentConfig.ID != releasedBrickID {
		t.Fatal("Current config not initialized with id.")
	}

	if !streamTesting.didRunSend {
		t.Fatal("Did not run send when setting new goal.")
	}

	if streamTesting.sendValue.GetLocation().X != expectedVector.X ||
		streamTesting.sendValue.GetLocation().Y != expectedVector.Y ||
		streamTesting.sendValue.GetLocation().Z != expectedVector.Z {

		t.Fatal("Invalid vector value set for new goal.")
	}

	if streamTesting.sendValue.GetReturnToBase() != expectedReturnToBase {
		t.Fatal("Invalid return to base set for new goal.")
	}
}

func TestUploadConfiguration(t *testing.T) {
	b := &Brain{}

	brick := &fakeBrickClient{}
	b.attachedBrick = brick

	b.uploadConfiguration(context.Background())

	if !brick.didUploadConfiguration {
		t.Fatal("Did not run upload configuration.")
	}
}
