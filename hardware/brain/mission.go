package brain

import (
	"encoding/json"
	"io/ioutil"
	"log"

	brick "leolaunches.com/assembly/brick"
)

type config struct {
}

type Mission struct {
	Environment      string                  `json:"environment"`
	DeliveryCapacity int                     `json:"delivery_capacity"`
	BricksToPlace    map[string]*brick.Brick `json:"bricks_queue"`

	IsComplete   bool                    `json:"is_complete,omitempty"`
	PlacedBricks map[string]*brick.Brick `json:"placed_bricks,omitempty"`
	currentBrick *brick.Brick            `json:"current_brick,omitempty"`
}

// func (m *Mission) AddBrick(brick *brick.Brick) {
// 	m.BricksToPlace = append(m.BricksToPlace, brick)
// }

func (m *Mission) PopBrick(id string) *brick.Brick {
	brick, exists := m.BricksToPlace[id]
	if m.size() < 1 || !exists {
		return nil
	}

	delete(m.BricksToPlace, id)
	return brick
}

func (m *Mission) size() int {
	return len(m.BricksToPlace)
}

func (m *Mission) AddPlacedBrick(b *brick.Brick) {
	if m.PlacedBricks == nil {
		m.PlacedBricks = make(map[string]*brick.Brick)
	}
	m.PlacedBricks[b.ID] = b

	if len(m.BricksToPlace) == 0 {
		m.IsComplete = true
	}
}

func LoadMissionFromFile(fn string) *Mission {
	configBytes, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Fatal("Unable to read config file.")
	}

	var configuration *Mission

	err = json.Unmarshal(configBytes, &configuration)

	if err != nil {
		log.Fatal("Unable to read unmarshal config file.", err)
	}

	return configuration
}

func (m *Mission) String() string {
	missionJSON, err := json.Marshal(m)

	if err != nil {
		log.Fatal("Failed to convert mission to string.")
	}

	return string(missionJSON)
}
