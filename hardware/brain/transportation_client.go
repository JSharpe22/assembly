package brain

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc/connectivity"

	"leolaunches.com/assembly/physics"

	"google.golang.org/grpc"
	api "leolaunches.com/assembly/api/v1"
)

const (
	transportationAddress = "127.0.0.1"
	transportationPort    = "50005"
)

type TransportationClient interface {
	Connect() TransportationClient
	Close()

	api.TransportServiceClient
}

type transportation struct {
	connection *grpc.ClientConn
	context    context.Context
	api.TransportServiceClient
}

func (t *transportation) Connect() TransportationClient {
	fullAddress := transportationAddress + ":" + transportationPort
	// Set up a connection to the server.
	conn, err := grpc.Dial(fullAddress, grpc.WithInsecure())
	for err != nil {
		log.Printf("did not connect: %v", err)
		time.Sleep(time.Second * 1)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	currentState := conn.GetState()
	for currentState != connectivity.Ready {
		if currentState == connectivity.TransientFailure {
			log.Println("did not connect. Trying again.")
			time.Sleep(time.Second * 1)
		}
		currentState = conn.GetState()
	}

	ts := &transportation{
		conn,
		ctx,
		api.NewTransportServiceClient(conn),
	}
	return ts
}

func (t *transportation) Close() {
	t.connection.Close()
}

func newGoal(vec *physics.Vector, returnToBase bool) *api.Goal {
	return &api.Goal{
		Location: &api.DestinationVector{
			X: vec.X,
			Y: vec.Y,
			Z: vec.Z,
		},
		ReturnToBase: returnToBase,
	}
}
