package brain

// import (
// 	"encoding/json"
// 	"io/ioutil"
// 	"log"
// 	"strings"
// 	"testing"

// 	"leolaunches.com/assembly/brick"

// 	"leolaunches.com/assembly/physics"
// )

// func loadTestMission() *Mission {
// 	configBytes, err := ioutil.ReadFile("test_config.json")
// 	if err != nil {
// 		log.Fatal("Unable to read config file.")
// 	}

// 	var configuration *Mission

// 	err = json.Unmarshal(configBytes, &configuration)

// 	if err != nil {
// 		log.Fatal("Unable to read unmarshal config file.", err)
// 	}

// 	return configuration
// }

// func loadTestMissionResults() string {
// 	configBytes, err := ioutil.ReadFile("test_config_results.json")
// 	if err != nil {
// 		log.Fatal("Unable to read config file.")
// 	}
// 	return string(configBytes)
// }

// func printMission(m *Mission) string {
// 	missionJSON, err := json.Marshal(m)

// 	if err != nil {
// 		log.Fatal("Failed to convert mission to string.")
// 	}

// 	return string(missionJSON)
// }

// type mockTransport struct {
// 	brain *Brain
// }

// func (mt *mockTransport) SetDestination(v *physics.Vector) {
// 	mt.brain.MarkGoalAchieved(v)
// }

// func (mt *mockTransport) GetDelivery() delivery {
// 	return &mockDelivery{}
// }

// func (mt *mockTransport) ReturnToBase() {
// 	mt.brain.MarkGoalAchieved(physics.NewVector(0, 0, 0))
// }

// type mockDelivery struct {
// }

// func (del *mockDelivery) RemoveBrick() *brick.Brick {
// 	return &brick.Brick{
// 		ID: "test",
// 	}
// }

// func TestEndToEnd(t *testing.T) {
// 	ts := &mockTransport{}
// 	b := New(ts)
// 	ts.brain = b
// 	b.LoadMission(loadTestMission())
// 	b.RunMission()

// 	missionStr := printMission(b.mission)

// 	expectedResults := loadTestMissionResults()
// 	equalResults := strings.Compare(missionStr, expectedResults)

// 	if equalResults != 0 {
// 		t.Log("Mission does not have expected results.")
// 		t.Errorf("Expected: %s.\n\nFound: %s.", expectedResults, missionStr)
// 	}
// }

// // var destinationSet bool

// // func TestNextGoal(t *testing.T) {
// // 	b := getBrick("0001")
// // 	b2 := getBrick("0002")
// // 	m := getTestMissionSingleBrick(b)
// // 	brain := getTestBrain(m)
// // 	brain.attachedNavSystem = getMockTransport(b2)
// // 	brain.nextGoal()

// // 	if brain.currentConfig == nil || b.ID != brain.currentConfig.ID {
// // 		t.Error("Current config not correctly set.")
// // 	}

// // 	if brain.mission.size() != 0 {
// // 		t.Error("Mission size not reducing.")
// // 	}

// // 	if brain.attachedBrick == nil || brain.attachedBrick.ID != b2.ID || brain.attachedBrick.ID == b.ID {
// // 		t.Error("attached brick not correctly set.")
// // 	}

// // 	if !destinationSet {
// // 		t.Error("New Destination not set.")
// // 	}

// // 	destinationSet = false
// // }

// // func TestBrickPlaced(t *testing.T) {
// // 	b := getBrick("0001")
// // 	b2 := getBrick("0002")
// // 	m := getTestMissionSingleBrick(b)
// // 	m.AddBrick(b2)
// // 	brain := getTestBrain(m)

// // 	b.
// // }

// // type mockTransport struct {
// // 	del *mockDelivery
// // }

// // func getMockTransport(b *brick.Brick) *mockTransport {
// // 	return &mockTransport{
// // 		del: getMockDelivery(b),
// // 	}
// // }

// // func (mT *mockTransport) SetDestination(*physics.Vector) {
// // 	destinationSet = true
// // }

// // func (mT *mockTransport) GetDelivery() delivery {
// // 	return mT.del
// // }

// // func (mT *mockTransport) ReturnToBase() {}

// // type mockDelivery struct {
// // 	b *brick.Brick
// // }

// // func getMockDelivery(b *brick.Brick) *mockDelivery {
// // 	return &mockDelivery{
// // 		b: b,
// // 	}
// // }

// // func (del *mockDelivery) RemoveBrick() *brick.Brick {
// // 	return del.b
// // }

// // func getBrick(id string) *brick.Brick {
// // 	return &brick.Brick{
// // 		ID: id,
// // 		LocationGoal: &physics.Vector{
// // 			X: 0,
// // 			Y: 0,
// // 			Z: 0,
// // 		},
// // 		Connections: map[brick.SideLocation]map[brick.ConnType][]string{
// // 			"0": map[brick.ConnType][]string{
// // 				"Electric": []string{
// // 					"4",
// // 				},
// // 			},
// // 		},
// // 	}
// // }

// // func getTestMissionSingleBrick(b *brick.Brick) *Mission {
// // 	return &Mission{
// // 		Environment:      "orbital",
// // 		DeliveryCapacity: 1,
// // 		isComplete:       false,
// // 		Bricks: []*brick.Brick{
// // 			b,
// // 		},
// // 	}
// // }

// // func getTestBrain(m *Mission) *Brain {
// // 	return &Brain{
// // 		attachedBrick:     nil,
// // 		currentConfig:     nil,
// // 		mission:           m,
// // 		attachedNavSystem: &mockTransport{},
// // 	}

// // }
