package brain

import (
	"context"
	"log"
	"time"

	"leolaunches.com/assembly/brick"

	"google.golang.org/grpc/connectivity"

	"google.golang.org/grpc"
	api "leolaunches.com/assembly/api/v1"
)

const (
	brickAddress = "127.0.0.1"
	brickPort    = "50135"
)

type BrickClient interface {
	Connect(id string) BrickClient
	Close()
	GetBrick() *brick.Brick
	api.BrickClient
}

type brickClient struct {
	connection   *grpc.ClientConn
	context      context.Context
	*brick.Brick `json:"brick"`
	api.BrickClient
}

func (bc *brickClient) Connect(id string) BrickClient {
	newBrick := brick.New(id)
	fullAddress := brickAddress + ":" + brickPort
	// Set up a connection to the server.
	conn, err := grpc.Dial(fullAddress, grpc.WithInsecure())
	for err != nil {
		log.Printf("did not connect: %v", err)
		time.Sleep(time.Second * 1)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	currentState := conn.GetState()
	for currentState != connectivity.Ready {
		if currentState == connectivity.TransientFailure {
			log.Println("did not connect. Trying again.")
			time.Sleep(time.Second * 1)
		}
		currentState = conn.GetState()
	}

	bc = &brickClient{
		conn,
		ctx,
		newBrick,
		api.NewBrickClient(conn),
	}

	return bc
}

func (bc *brickClient) Close() {
	bc.Brick.ShutDown()
	bc.connection.Close()
}

func (bc *brickClient) GetBrick() *brick.Brick {
	return bc.Brick
}
