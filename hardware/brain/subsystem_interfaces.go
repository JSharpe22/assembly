package brain

import (
	"leolaunches.com/assembly/brick"
	"leolaunches.com/assembly/physics"
)

type transport interface {
	SetDestination(*physics.Vector)
	GetDelivery() delivery
	ReturnToBase()
}

type delivery interface {
	RemoveBrick() *brick.Brick
}

// brick? Or composition
