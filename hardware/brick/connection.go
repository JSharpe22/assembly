package brick

const (
	Front   SideLocation = "0"
	Top     SideLocation = "1"
	Right   SideLocation = "2"
	Back    SideLocation = "3"
	Bottom  SideLocation = "4"
	Left    SideLocation = "5"
	Invalid SideLocation = "INVALID"
)

type ConnType string

var Electric ConnType
var Insulation ConnType
var Chemical ConnType

type SideLocation string

type Connection struct {
	BrickSrc                 *Brick
	BrickToConnect           *Brick
	ConnectionInfo           *ConnectedBrick
	LocationBrickSrc         SideLocation
	LocationOnBrickToConnect SideLocation
}

type ConnectedBrick struct {
	BrickID             string       `json:"brick_id"`
	LocationConnectedTo SideLocation `json:"connected_brick_location"`
	AngleConnected      float64      `json:"angle"`
	isLocked            bool
	brick               *Brick
}
