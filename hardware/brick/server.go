package brick

import (
	"context"
	"encoding/json"
	"log"
	"net"

	"google.golang.org/grpc"

	api "leolaunches.com/assembly/api/v1"
)

const (
	address = "127.0.0.1"
	port    = "50135"
)

type BrickServer struct {
	system *Brick
	server *grpc.Server
}

func StartServer(system *Brick) *BrickServer {
	server := &BrickServer{
		system: system,
	}

	fullAddress := address + ":" + port

	lis, err := net.Listen("tcp", fullAddress)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	api.RegisterBrickServer(s, server)

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	server.server = s
	return server
}

func (bs *BrickServer) StopServer() {
	bs.server.GracefulStop()
}

func (bs *BrickServer) UploadConfiguration(ctx context.Context, config *api.Configuration) (*api.UploadStatus, error) {
	newBrick, err := convertConfigToBrick(config)
	if newBrick == nil || err != nil {
		return &api.UploadStatus{
			Success: false,
		}, err
	}

	bs.system.uploadConfiguration(newBrick)

	return &api.UploadStatus{
		Success: true,
	}, nil
}

func (bs *BrickServer) ConnectBrick(ctx context.Context, config *api.ConnectionConfiguration) (*api.ConnectionStatus, error) {
	newConnection, err := convertConnectionConfigToObject(config)
	if newConnection == nil || err != nil {
		return &api.ConnectionStatus{
			Success: false,
		}, err
	}

	bs.system.connectBrick(newConnection)

	return &api.ConnectionStatus{
		Success: true,
	}, nil
}

func GetGRPCConfiguration(brick *Brick) (*api.Configuration, error) {
	data, err := json.Marshal(brick)

	if err != nil {
		return nil, err
	}
	return &api.Configuration{SerializedInfo: string(data)}, nil
}

func convertConfigToBrick(config *api.Configuration) (*Brick, error) {
	newBrick := &Brick{}
	err := json.Unmarshal([]byte(config.SerializedInfo), newBrick)

	if err != nil {
		log.Printf("Error unmarshalling config. %v", err)
		return nil, err
	}

	return newBrick, nil
}

func GetGRPCConnectionConfiguration(conn *Connection) (*api.ConnectionConfiguration, error) {
	data, err := json.Marshal(conn)

	if err != nil {
		return nil, err
	}
	return &api.ConnectionConfiguration{SerializedInfo: string(data)}, nil
}

func convertConnectionConfigToObject(config *api.ConnectionConfiguration) (*Connection, error) {
	newConnection := &Connection{}
	err := json.Unmarshal([]byte(config.SerializedInfo), newConnection)

	if err != nil {
		log.Printf("Error unmarshalling config. %v", err)
		return nil, err
	}

	return newConnection, nil
}
