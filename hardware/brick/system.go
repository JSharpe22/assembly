package brick

import (
	"leolaunches.com/assembly/physics"
)

const (
	defaultLength = 1
	defaultDepth  = 0.25
	defaultHeight = 1

	numberOfSides = 6
)

type Brick struct {
	// Configuration Data
	ID              string                                 `json:"id"`
	ConnectedBricks map[SideLocation]*ConnectedBrick       `json:"connected_bricks"`
	Connections     map[SideLocation]map[ConnType][]string `json:"connections"`
	LocationGoal    *physics.Vector                        `json:"location_goal"`

	server *BrickServer

	// DroneConnector

	// physical attributes
	height             float64
	length             float64
	depth              float64
	*physics.Kinematic `json:"kinematic"`
}

func New(id string) *Brick {
	b := &Brick{
		ID:     id,
		height: defaultHeight,
		length: defaultLength,
		depth:  defaultDepth,
	}
	b.server = StartServer(b)
	return b
}

func (b *Brick) ShutDown() {
	b.server.StopServer()
}

func (b *Brick) SetDims(id string, length, depth float64) {
	b.height = length
	b.length = length
	b.depth = depth
}

func (b *Brick) uploadConfiguration(config *Brick) {
	// Go through config and set all lock and connections accordingly.
	b.ID = config.ID
	b.Connections = config.Connections
}

func (b *Brick) connectBrick(connection *Connection) {

	// check all brick connected already. If already connected, exit
	for _, brick := range b.ConnectedBricks {
		if brick.BrickID == connection.BrickToConnect.ID {
			return
		}
	}

	// if this is first brick to be connected, initialize
	if b.ConnectedBricks == nil {
		b.ConnectedBricks = make(map[SideLocation]*ConnectedBrick)
	}

	connection.ConnectionInfo.brick = connection.BrickToConnect
	b.ConnectedBricks[connection.LocationOnBrickToConnect] = connection.ConnectionInfo
	b.ConnectedBricks[connection.LocationOnBrickToConnect].isLocked = true

	// connectedInfo.brick = attachedBrick // What is this?

	newConnectedInfo := &ConnectedBrick{
		BrickID:             connection.BrickToConnect.ID,
		LocationConnectedTo: connection.LocationBrickSrc,
		AngleConnected:      connection.ConnectionInfo.AngleConnected * -1,
	}

	newConnection := &Connection{
		BrickSrc:                 connection.BrickToConnect,
		BrickToConnect:           b,
		ConnectionInfo:           newConnectedInfo,
		LocationBrickSrc:         connection.LocationOnBrickToConnect,
		LocationOnBrickToConnect: connection.LocationBrickSrc,
	}

	connection.BrickToConnect.connectBrick(newConnection)

}

// Connected bricks should be empty until brick is actually added.
// Brain should check which have been added then check configuraiton to determine which to add as connected and lock.
