package physics

// Kinematic represents an object motion state.
type Kinematic struct {
	mass   float64
	volume float64
	time   float64

	rotation *Quaternion //TODO need to add angular force, acceleration, velocity

	location     *Vector
	velocity     *Vector
	acceleration *Vector
	force        *Vector
	oldForce     *Vector

	env         *Environment
	parent      *Kinematic
	parentIndex int
	children    []*Kinematic
}

// NewKinematic instantiates a Kinematic object with the fields initiailzed to base values.
func NewKinematic(mass float64) *Kinematic {
	return &Kinematic{
		mass:         mass,
		volume:       0,
		rotation:     &Quaternion{},
		location:     NewVector(0, 0, 0),
		velocity:     NewVector(0, 0, 0),
		acceleration: NewVector(0, 0, 0),
		force:        NewVector(0, 0, 0),
		oldForce:     NewVector(0, 0, 0),
		children:     []*Kinematic{},
	}
}

// SetForce sets the force of the kinametic for next update.
func (k *Kinematic) SetForce(f *Vector) {
	k.force = f
}

// Update progresses the object forward.
func (k *Kinematic) Update(deltaTime float64) {
	k.time = deltaTime + k.time

	averagedForce := k.force.Add(k.oldForce).DivideScalar(2.0)
	k.oldForce = k.force

	oldAcceleration := k.acceleration
	k.acceleration = averagedForce.DivideScalar(k.mass)

	averagedAcceleration := k.acceleration.Add(oldAcceleration).DivideScalar(2.0)
	oldVelocity := k.velocity
	k.velocity = oldVelocity.Add(averagedAcceleration.MultiplyScalar(deltaTime))

	averagedVelocity := k.velocity.Add(oldVelocity).DivideScalar(2.0)
	oldLocation := k.location
	k.location = oldLocation.Add(averagedVelocity.MultiplyScalar(deltaTime))

	for _, child := range k.children {
		child.SetForce(k.force) //TODO Eventually want to include rotation which will requrie relative positioning.
		child.Update(deltaTime)
	}
}

// AddChild inserts a new child into the list of children and handles setting parent.
func (k *Kinematic) AddChild(newChild *Kinematic) {
	k.children = append(k.children, newChild)
	newChild.parent = k
	newChild.parentIndex = len(k.children) - 1
}

// RemoveChild removes a child kinematic object from the existing children list.
func (k *Kinematic) RemoveChild(index int) {
	k.children = append(k.children[:index], k.children[index+1:]...) // Not intended for large child sizes.
}

// Quaternion is a structure that represents rotation. Full implementation will be developed later.
type Quaternion struct {
	a float64
	b float64
	c float64
	d float64
}
