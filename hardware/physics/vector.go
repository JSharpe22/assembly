package physics

import (
	"fmt"
)

const comparePrecision = 0.001

type Vector struct {
	X float64
	Y float64
	Z float64
}

func NewVector(x, y, z float64) *Vector {
	return &Vector{X: x, Y: y, Z: z}
}

func (v *Vector) Add(v2 *Vector) *Vector {
	return &Vector{
		X: v.X + v2.X,
		Y: v.Y + v2.Y,
		Z: v.Z + v2.Z,
	}
}

func (v *Vector) Subtract(v2 *Vector) *Vector {
	return &Vector{
		X: v.X - v2.X,
		Y: v.Y - v2.Y,
		Z: v.Z - v2.Z,
	}
}

func (v *Vector) MultiplyScalar(scalar float64) *Vector {
	return &Vector{
		X: v.X * scalar,
		Y: v.Y * scalar,
		Z: v.Z * scalar,
	}
}

func (v *Vector) DivideScalar(scalar float64) *Vector {
	return &Vector{
		X: v.X / scalar,
		Y: v.Y / scalar,
		Z: v.Z / scalar,
	}
}

func (v *Vector) CompareVector(v2 *Vector) bool {
	return (v.X-v2.X <= comparePrecision && v2.X-v.X <= comparePrecision) &&
		(v.Y-v2.Y <= comparePrecision && v2.Y-v.Y <= comparePrecision) &&
		(v.Z-v2.Z <= comparePrecision && v2.Z-v.Z <= comparePrecision)
}

func (v *Vector) String() string {
	return fmt.Sprintf("<%f, %f, %f>", v.X, v.Y, v.Z)
}
