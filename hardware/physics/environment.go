package physics

import (
	"encoding/json"
	"log"
)

type Object interface {
	Update(dt float64)
	// GetChildren() []Object
}

// Environment is used as the "world" or controller for the simulation.
type Environment struct {
	objects      map[string]Object
	recordedData map[float64]map[string]string // map of time to map of objectName to object data.
	passedTime   float64

	isDone bool
	// done   chan bool
}

// NewEnvironment creates a blank environment initializing all required variables.
func NewEnvironment() *Environment {
	return &Environment{
		objects:      make(map[string]Object),
		passedTime:   0,
		isDone:       false,
		recordedData: map[float64]map[string]string{},
		// done:       make(chan bool),
	}
}

// AddObject adds an object to the world that will need to be updated.
func (e *Environment) AddObject(key string, k Object) {
	e.objects[key] = k
}

// Update will iterate through each object and update the physics for them.
func (e *Environment) Update(dt float64) {
	for {
		// Check if all objects are done.
		if e.isDone {
			// <-e.done
			break
		}

		// Loop through and update
		for key, obj := range e.objects {
			if e.recordedData[e.passedTime] == nil {
				e.recordedData[e.passedTime] = map[string]string{}
			}

			obj.Update(dt)

			data, err := json.Marshal(obj)
			if err != nil {
				log.Fatal("Unable to package data. No idea what to do.")
			}
			e.recordedData[e.passedTime][key] = string(data)
			e.passedTime += dt
		}
	}
}

func (e *Environment) MarkDone() {
	e.isDone = true
}

func (e *Environment) GetReportData() (string, error) {
	data, err := json.Marshal(e.recordedData)
	return string(data), err
}

type OrbitalPhysics struct {
}

type TerrestrialPhysics struct {
}
