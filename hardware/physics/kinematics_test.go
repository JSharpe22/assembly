package physics

import "testing"

func TestUpdate(t *testing.T) {
	deltaTime := 0.5

	startParameters := &Kinematic{
		mass:         10,
		volume:       1,
		time:         10,
		rotation:     nil,
		location:     NewVector(43.301, 43.301, 43.301),
		velocity:     NewVector(5.773, 5.773, 5.773),
		acceleration: NewVector(0.289, 0.289, 0.289),
		force:        NewVector(2.89, 2.89, 2.89),
		oldForce:     NewVector(2.89, 2.89, 2.89),
	}

	expectedResultsTestA := &Kinematic{
		mass:         10,
		volume:       1,
		time:         startParameters.time + deltaTime,
		rotation:     nil,
		location:     NewVector(46.227, 46.227, 46.227),
		velocity:     NewVector(5.932, 5.932, 5.932),
		acceleration: NewVector(0.3464, 0.3464, 0.3464),
		force:        NewVector(4.04, 4.04, 4.04),
		oldForce:     NewVector(2.89, 2.89, 2.89),
	}

	startParameters.force = NewVector(4.04, 4.04, 4.04)
	startParameters.Update(deltaTime)

	if startParameters.mass != expectedResultsTestA.mass {
		t.Errorf("Mass not equivalent after update. See: %f, expect: %f", startParameters.mass, expectedResultsTestA.mass)
	}

	if startParameters.volume != expectedResultsTestA.volume {
		t.Errorf("Volume not equivalent after update. See: %f, expect: %f", startParameters.volume, expectedResultsTestA.volume)
	}

	if startParameters.time != expectedResultsTestA.time {
		t.Errorf("Time not equivalent after update. See: %f, expect: %f", startParameters.time, expectedResultsTestA.time)
	}

	if !startParameters.location.CompareVector(expectedResultsTestA.location) {
		t.Errorf("Location not equivalent after update. See: %s, expect: %s", startParameters.location, expectedResultsTestA.location)
	}

	if !startParameters.velocity.CompareVector(expectedResultsTestA.velocity) {
		t.Errorf("velocity not equivalent after update. See: %s, expect: %s", startParameters.velocity, expectedResultsTestA.velocity)
	}

	if !startParameters.acceleration.CompareVector(expectedResultsTestA.acceleration) {
		t.Errorf("acceleration not equivalent after update. See: %s, expect: %s", startParameters.acceleration, expectedResultsTestA.acceleration)
	}

	if !startParameters.force.CompareVector(expectedResultsTestA.force) {
		t.Errorf("force not equivalent after update. See: %s, expect: %s", startParameters.force, expectedResultsTestA.force)
	}

	startParameters = &Kinematic{
		mass:         10,
		volume:       1,
		time:         10,
		rotation:     nil,
		location:     NewVector(75, 0, 0),
		velocity:     NewVector(10, 0, 0),
		acceleration: NewVector(0.5, 0, 0),
		force:        NewVector(5, 0, 0),
		oldForce:     NewVector(5, 0, 0),
	}

	deltaTime = 2

	expectedResultsTestB := &Kinematic{
		mass:         10,
		volume:       1,
		time:         startParameters.time + deltaTime,
		rotation:     nil,
		location:     NewVector(96.1, 0, 0),
		velocity:     NewVector(11.1, 0, 0),
		acceleration: NewVector(0.6, 0, 0),
		force:        NewVector(7, 0, 0),
		oldForce:     NewVector(5, 0, 0),
	}

	startParameters.force = NewVector(7, 0, 0)
	startParameters.Update(deltaTime)

	if startParameters.mass != expectedResultsTestB.mass {
		t.Errorf("Mass not equivalent after update. See: %f, expect: %f", startParameters.mass, expectedResultsTestB.mass)
	}

	if startParameters.volume != expectedResultsTestB.volume {
		t.Errorf("Volume not equivalent after update. See: %f, expect: %f", startParameters.volume, expectedResultsTestB.volume)
	}

	if startParameters.time != expectedResultsTestB.time {
		t.Errorf("Time not equivalent after update. See: %f, expect: %f", startParameters.time, expectedResultsTestB.time)
	}

	if !startParameters.location.CompareVector(expectedResultsTestB.location) {
		t.Errorf("Location not equivalent after update. See: %s, expect: %s", startParameters.location, expectedResultsTestB.location)
	}

	if !startParameters.velocity.CompareVector(expectedResultsTestB.velocity) {
		t.Errorf("velocity not equivalent after update. See: %s, expect: %s", startParameters.velocity, expectedResultsTestB.velocity)
	}

	if !startParameters.acceleration.CompareVector(expectedResultsTestB.acceleration) {
		t.Errorf("acceleration not equivalent after update. See: %s, expect: %s", startParameters.acceleration, expectedResultsTestB.acceleration)
	}

	if !startParameters.force.CompareVector(expectedResultsTestB.force) {
		t.Errorf("force not equivalent after update. See: %s, expect: %s", startParameters.force, expectedResultsTestB.force)
	}

}
