package main

import (
	"log"

	"leolaunches.com/assembly/brain"
)

func main() {

	log.SetPrefix("[** Brain ***********] ")

	// Create brain module configured as needed.
	b := brain.NewBrain()
	log.Println("Initialized brain.")

	// Load mission from file.
	mission := brain.LoadMissionFromFile("./configs/basic_config.json")
	b.LoadMission(mission)
	log.Println("Mission loaded.")

	// Connect to client
	b.ConnectToTransport()
	log.Println("Connection successful.")

	// Run mission
	b.RunMission()
	log.Println("mission started.")
}
