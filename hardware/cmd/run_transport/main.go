package main

import (
	"log"
	"net"

	"leolaunches.com/assembly/transport"

	"google.golang.org/grpc"
	api "leolaunches.com/assembly/api/v1"
)

const (
	port = ":50005"
)

func main() {
	// create transportation system
	transportation := &transport.TransportSystem{
		//TODO Add parameters eventually
	}
	log.SetPrefix("[** Transportation **] ")
	log.Println("Configured transport module.")

	// create listiner
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// create grpc server
	s := grpc.NewServer()
	transportServer := &transport.TransportServer{
		System: transportation,
	}
	// transportation.server = transportServer
	api.RegisterTransportServiceServer(s, transportServer)
	log.Printf("Created server. Running on Port %s", port)

	// and start...
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	log.Println("Test.")
	// x := s.GetServiceInfo()
	// log.Printf("Test: %+v", x)
	// if err := s.Serve(lis); err != nil {
	// 	log.Fatalf("failed to serve: %v", err)
	// }

	s.Stop()

	for {
	}
}
