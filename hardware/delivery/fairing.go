package delivery

import (
	"leolaunches.com/assembly/physics"
)

type Fairing struct {
	*delivery
}

func NewFairing() *Fairing {
	return &Fairing{
		delivery: &delivery{
			Kinematic: physics.NewKinematic(50),
		},
	}
}
