package delivery

import (
	"leolaunches.com/assembly/brick"
	"leolaunches.com/assembly/physics"
)

type delivery struct {
	bricks          []brick.Brick
	drone           transport // Create interface later
	brickCapacity   int       `json:"brick_capacity"`
	bricksRemaining int       `json:"bricks_remaining"`

	*physics.Kinematic `json:"kinematic"`
}

func (d *delivery) StoreBricks(numberOfBricks int) {
	d.bricks = make([]brick.Brick, numberOfBricks)
	d.brickCapacity = numberOfBricks
	d.bricksRemaining = numberOfBricks
}

func (d *delivery) RemoveBrick() *brick.Brick {
	if d.bricksRemaining < 1 {
		return nil
	}

	d.bricksRemaining = d.bricksRemaining - 1
	d.bricks = d.bricks[:d.bricksRemaining]
	return &d.bricks[d.bricksRemaining]
}

func (d *delivery) StoreTransportSystem(ts transport) {
	d.drone = ts
}

func (d *delivery) RemoveTransportSystem() (ts transport) {
	leavingDrone := d.drone
	d.drone = nil
	return leavingDrone
}

// Does action of sending & recieving signals that will be used to determine location
func (d *delivery) pingDrone() {

}

// Called by drone to trigger updating location. Takes care of math.
func (d *delivery) CalculateLocation() {

}

// Once the signals and calculations are complete, an update will be sent to the drone.
func (d *delivery) sendUpdatedLocation() {

}

func (d *delivery) Update(dt float64) {
	d.Kinematic.Update(dt)
}

// func (d *delivery) GetReportData() string {
// 	data := map[string]string{}

// 	data["brick_capacity"] = d.brickCapacity
// 	data["bricks_remaining"] = d.bricksRemaining
// 	data["kinematics"] =
// }
