package transport

type OribtalDrone struct {
	*TransportSystem
}

func NewDrone() *OribtalDrone {
	return &OribtalDrone{New()}
}

func (d *OribtalDrone) RespondToPing() {}
