package transport

import (
	"context"
	"io"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"

	api "leolaunches.com/assembly/api/v1"
	"leolaunches.com/assembly/physics"
)

const (
	address = "127.0.0.1"
	port    = "50005"
)

type TransportServer struct {
	system     *TransportSystem
	goalServer api.TransportService_SetGoalServer
	server     *grpc.Server
}

func StartServer(system *TransportSystem) *TransportServer {
	server := &TransportServer{
		system: system,
	}

	fullAddress := address + ":" + port

	lis, err := net.Listen("tcp", fullAddress)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	api.RegisterTransportServiceServer(s, server)

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	server.server = s
	return server
}

func (ts *TransportServer) StopServer() {
	ts.server.GracefulStop()
}

func (ts *TransportServer) SetGoal(setGoalServer api.TransportService_SetGoalServer) error {

	ctx := setGoalServer.Context()

	if ts.goalServer == nil {
		ts.goalServer = setGoalServer
	}

	for {

		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		req, err := setGoalServer.Recv()
		if err == io.EOF {
			// return will close stream from server side
			return nil
		}
		if err != nil {
			log.Printf("receive error %v", err)
			continue
		}

		returnToBase := req.GetReturnToBase()
		log.Printf("Setting goal return to base: %v.", returnToBase)

		if returnToBase {
			ts.system.returnToBase()
			continue
		}

		loc := req.GetLocation()
		newVec := convertVector(*loc)
		log.Printf("Setting goal return to base: %s.", newVec.String())
		ts.system.setDestination(newVec)
		continue
	}
}

func (ts *TransportServer) MarkGoalAchieved() {
	log.Println("Marking goal complete.")

	if ts.goalServer == nil {
		log.Println("setGoalServer not set.")
	}

	ts.goalServer.Send(&api.GoalResults{
		Success: true,
	})
}

func convertVector(vec api.DestinationVector) *physics.Vector {
	return &physics.Vector{
		vec.X,
		vec.Y,
		vec.Z,
	}
}

func (ts *TransportServer) ReleaseBrick(ctx context.Context, cmd *api.ReleaseBrickCommand) (*api.BrickReleased, error) {
	log.Println("Releasing brick.")
	brickID := ts.system.releaseBrick()

	return &api.BrickReleased{
		Id: brickID,
	}, nil
}

func (ts *TransportServer) Ping(info api.PingInformation) api.PingResponse {
	return api.PingResponse{
		Id:        info.Id,
		Timestamp: time.Now().String(),
	}
}
