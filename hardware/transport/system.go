package transport

import (
	"leolaunches.com/assembly/physics"
)

var brickIDs = []string{
	"5395",
	"5394",
}

type TransportSystem struct {
	energyCapacity        float64 //  J
	fullThrottlePowerRate float64 // J/s
	fullThrottleForce     float64 // N

	// brain              *brain.Brain
	currentDestination *physics.Vector
	isAutonomyMode     bool
	homeBase           delivery

	server *TransportServer

	*physics.Kinematic
}

func New() *TransportSystem {
	ts := &TransportSystem{}
	ts.currentDestination = physics.NewVector(0, 0, 0)
	server := StartServer(ts)
	ts.server = server
	ts.Kinematic = physics.NewKinematic(0)
	return ts
}

func (ts *TransportSystem) ShutDown() {
	ts.server.StopServer()
}

func (ts *TransportSystem) getDelivery() delivery {
	return ts.homeBase
}

func (ts *TransportSystem) returnToBase() {
	ts.currentDestination = physics.NewVector(0, 0, 0)
	ts.markGoalAchieved() //TODO move to the "move" function so it checks everytime updated.
}

// func (ts *TransportSystem) AttachBrain(brain *brain.Brain) {
// 	ts.brain = brain
// }

// Will use algorithm to determine best location eventually. For now will just do straight line.
func (ts *TransportSystem) setDestination(destination *physics.Vector) {
	ts.currentDestination = destination
	ts.isAutonomyMode = true
	ts.markGoalAchieved() //TODO move to the "move" function so it checks everytime updated.
	// ts.brain.MarkGoalAchieved(ts.currentDestination)
}

func (ts *TransportSystem) markGoalAchieved() {
	ts.server.MarkGoalAchieved()
}

// Will add this option later. "Manual" control of nav.
// func (ts *TransportSystem) Move(direction *physics.Vector, percentThrottle float64) {
// }

func (ts *TransportSystem) requestLocation() {

}

func (ts *TransportSystem) releaseBrick() string {
	if len(brickIDs) == 0 {
		return ""
	}
	currBrickID := brickIDs[0]
	brickIDs = brickIDs[1:]
	return currBrickID
	// return ts.homeBase.ReleaseBrick()
}

func (ts *TransportSystem) Update(dt float64) {
	ts.Kinematic.Update(dt)
}
