package transport

type delivery interface {
	CalculateLocation()
	ReleaseBrick() string
}
