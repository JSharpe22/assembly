package physics

import (
	"testing"
)

const (
	VectorX = 0
	VectorY = 1
	VectorZ = 2
)

var Vector1 = []float64{5, 12, 13}
var Vector2 = []float64{3.3, 6.6, 9.9}

func TestAdd(t *testing.T) {
	vectorA := NewVector(Vector1[VectorX], Vector1[VectorY], Vector1[VectorZ])
	vectorB := NewVector(Vector2[VectorX], Vector2[VectorY], Vector2[VectorZ])

	result := vectorA.Add(vectorB)

	if result.X != (Vector1[VectorX] + Vector2[VectorX]) {
		t.Errorf("Vector not adding correctly. See: %f, expect: %f", result.X, (Vector1[VectorX] + Vector2[VectorX]))
	}

	if result.Y != (Vector1[VectorY] + Vector2[VectorY]) {
		t.Errorf("Vector not adding correctly. See: %f, expect: %f", result.Y, (Vector1[VectorY] + Vector2[VectorY]))
	}

	if result.Z != (Vector1[VectorZ] + Vector2[VectorZ]) {
		t.Errorf("Vector not adding correctly. See: %f, expect: %f", result.Z, (Vector1[VectorZ] + Vector2[VectorZ]))
	}
}

func TestSubtract(t *testing.T) {
	vectorA := NewVector(Vector1[VectorX], Vector1[VectorY], Vector1[VectorZ])
	vectorB := NewVector(Vector2[VectorX], Vector2[VectorY], Vector2[VectorZ])

	result := vectorA.Subtract(vectorB)

	if result.X != (Vector1[VectorX] - Vector2[VectorX]) {
		t.Errorf("Vector not subtracting correctly. See: %f, expect: %f", result.X, (Vector1[VectorX] - Vector2[VectorX]))
	}

	if result.Y != (Vector1[VectorY] - Vector2[VectorY]) {
		t.Errorf("Vector not subtracting correctly. See: %f, expect: %f", result.Y, (Vector1[VectorY] - Vector2[VectorY]))
	}

	if result.Z != (Vector1[VectorZ] - Vector2[VectorZ]) {
		t.Errorf("Vector not subtracting correctly. See: %f, expect: %f", result.Z, (Vector1[VectorZ] - Vector2[VectorZ]))
	}
}

func TestMultiplyScalar(t *testing.T) {
	vectorA := NewVector(Vector1[VectorX], Vector1[VectorY], Vector1[VectorZ])
	scalar := 5.0

	result := vectorA.MultiplyScalar(scalar)

	if result.X != (Vector1[VectorX] * scalar) {
		t.Errorf("Vector not multiplying correctly. See: %f, expect: %f", result.X, (Vector1[VectorX] * scalar))
	}

	if result.Y != (Vector1[VectorY] * scalar) {
		t.Errorf("Vector not multiplying correctly. See: %f, expect: %f", result.Y, (Vector1[VectorY] * scalar))
	}

	if result.Z != (Vector1[VectorZ] * scalar) {
		t.Errorf("Vector not multiplying correctly. See: %f, expect: %f", result.Z, (Vector1[VectorZ] * scalar))
	}
}

func TestDivideScalar(t *testing.T) {
	vectorA := NewVector(Vector1[VectorX], Vector1[VectorY], Vector1[VectorZ])
	scalar := 5.0

	result := vectorA.DivideScalar(scalar)

	if result.X != (Vector1[VectorX] / scalar) {
		t.Errorf("Vector not dividing correctly. See: %f, expect: %f", result.X, (Vector1[VectorX] / scalar))
	}

	if result.Y != (Vector1[VectorY] / scalar) {
		t.Errorf("Vector not dividing correctly. See: %f, expect: %f", result.Y, (Vector1[VectorY] / scalar))
	}

	if result.Z != (Vector1[VectorZ] / scalar) {
		t.Errorf("Vector not dividing correctly. See: %f, expect: %f", result.Z, (Vector1[VectorZ] / scalar))
	}
}

func TestCompare(t *testing.T) {
	difference := 5.0

	vectorA := NewVector(5.352, 2.382, 48)
	vectorB := NewVector(vectorA.X+(0.8*comparePrecision), vectorA.Y-(0.5*comparePrecision), vectorA.Z)

	if !vectorA.CompareVector(vectorB) {
		t.Errorf("vectorA and vectorB should be equal. They are A: %s, B: %s.", vectorA, vectorB)
	}

	vectorB.X = vectorB.X + difference

	if vectorA.CompareVector(vectorB) {
		t.Errorf("vectorA and vectorB should not be equal. They are A: %s, B: %s.", vectorA, vectorB)
	}

	vectorB.X = vectorB.X - difference
	vectorB.Y = vectorB.Y + difference

	if vectorA.CompareVector(vectorB) {
		t.Errorf("vectorA and vectorB should not be equal. They are A: %s, B: %s.", vectorA, vectorB)
	}

	vectorB.Y = vectorB.Y - difference
	vectorB.Z = vectorB.Z + difference

	if vectorA.CompareVector(vectorB) {
		t.Errorf("vectorA and vectorB should not be equal. They are A: %s, B: %s.", vectorA, vectorB)
	}

}
