package physics

// Kinematic represents an object motion state.
type Kinematic struct {
	Mass   float64
	Volume float64
	Time   float64

	Rotation *Quaternion //TODO need to add angular force, acceleration, velocity

	Location     *Vector
	Velocity     *Vector
	Acceleration *Vector
	Force        *Vector
	oldForce     *Vector

	env         *Environment
	parent      *Kinematic
	parentIndex int
	children    []*Kinematic
}

// NewKinematic instantiates a Kinematic object with the fields initiailzed to base values.
func NewKinematic(mass float64) *Kinematic {
	return &Kinematic{
		Mass:         mass,
		Volume:       0,
		Time:         0,
		Rotation:     &Quaternion{},
		Location:     NewVector(0, 0, 0),
		Velocity:     NewVector(0, 0, 0),
		Acceleration: NewVector(0, 0, 0),
		Force:        NewVector(0, 0, 0),
		oldForce:     NewVector(0, 0, 0),
		children:     []*Kinematic{},
	}
}

// SetForce sets the force of the kinametic for next update.
func (k *Kinematic) SetForce(f *Vector) {
	k.Force = f
}

// Update progresses the object forward.
func (k *Kinematic) Update(deltaTime float64) {
	k.Time = deltaTime + k.Time

	averagedForce := k.Force.Add(k.oldForce).DivideScalar(2.0)
	k.oldForce = k.Force

	oldAcceleration := k.Acceleration
	k.Acceleration = averagedForce.DivideScalar(k.Mass)

	averagedAcceleration := k.Acceleration.Add(oldAcceleration).DivideScalar(2.0)
	oldVelocity := k.Velocity
	k.Velocity = oldVelocity.Add(averagedAcceleration.MultiplyScalar(deltaTime))

	averagedVelocity := k.Velocity.Add(oldVelocity).DivideScalar(2.0)
	oldLocation := k.Location
	k.Location = oldLocation.Add(averagedVelocity.MultiplyScalar(deltaTime))

	for _, child := range k.children {
		child.SetForce(k.Force) //TODO Eventually want to include rotation which will requrie relative positioning.
		child.Update(deltaTime)
	}
}

// AddChild inserts a new child into the list of children and handles setting parent.
func (k *Kinematic) AddChild(newChild *Kinematic) {
	k.children = append(k.children, newChild)
	newChild.parent = k
	newChild.parentIndex = len(k.children) - 1
}

// RemoveChild removes a child kinematic object from the existing children list.
func (k *Kinematic) RemoveChild(index int) {
	k.children = append(k.children[:index], k.children[index+1:]...) // Not intended for large child sizes.
}

// Quaternion is a structure that represents rotation. Full implementation will be developed later.
type Quaternion struct {
	a float64
	b float64
	c float64
	d float64
}
