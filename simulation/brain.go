package simulation

import (
	"leolaunches.com/assembly/physics"
)

const (
	waiting          State = "waiting"
	pickingUpBrick   State = "picking_up_brick"
	settingGoal      State = "set_goal"
	movingToGoal     State = "move_to_goal"
	configuringBrick State = "configure_brick"
	returningToBase  State = "return_to_base"
	complete         State = "complete"
	unknown          State = "unknown"

	timeToPickUpBrick    = 0.1
	timeToSetGoal        = 0.05
	timePerMoveUpdate    = 0.1
	timeToConfigureBrick = 1
)

// Brain stands for (Br)ick (A)rm and (In)structor
// This object will be in charge of talking to transport system
// to instruct it where it needs to be moved to place the current brick.
// Once placed, the brain will upload a configuration to the brick and lock
// the brick once connected correctly.
type brain struct {
	CurrentConfig *brick `json:"current_config,omitempty"`
	// state         State
	// arm           *Arm

	transportation transport
	AttachedBrick  *brick `json:"attached_brick,omitempty"`

	mission *Mission

	*physics.Kinematic `json:"kinematic,omitempty"`
	Env                *physics.Environment
}

func newBrain() *brain {
	// Eventuially need to load arm.
	return &brain{
		Kinematic: physics.NewKinematic(0),
	}
}

func (b *brain) loadMission(m *Mission) {
	b.mission = m
}

func (b *brain) Update() {
	nextState := unknown
	var currFunction Action
	var latestResult *Result
	var timeElapsed float64

	for b.mission.state != complete {
		switch b.mission.state {
		case waiting:
			break
		case pickingUpBrick:
			nextState = settingGoal
			timeElapsed = timeToPickUpBrick
			currFunction = b.pickUpBrick
			break
		case settingGoal:
			nextState = movingToGoal
			timeElapsed = timeToSetGoal
			currFunction = b.setGoal
			break
		case movingToGoal:
			nextState = configuringBrick
			timeElapsed = timePerMoveUpdate
			currFunction = b.moveToGoal
			break
		case configuringBrick:
			nextState = returningToBase
			timeElapsed = timeToConfigureBrick
			currFunction = b.configureBrick
			break
		case returningToBase:
			if len(b.mission.BricksToPlace) == 0 {
				nextState = complete
				break
			}

			nextState = pickingUpBrick
			timeElapsed = timePerMoveUpdate
			currFunction = b.returnToBase
			break
		case complete:
		default:
			nextState = unknown
			break
		}
	}

	if nextState == complete {
		return
	}

	latestResult = currFunction()
	if latestResult.isComplete {
		b.mission.state = nextState
	}
	b.mission.timeRunning += timeElapsed
}

func (b *brain) pickUpBrick() *Result {
	b.AttachedBrick = b.transportation.homeBase.RemoveBrick()
	b.CurrentConfig = b.mission.PopBrickConfiguration()

	return &Result{
		log:           "New brick picked up.",
		isComplete:    true,
		transportData: b.transportation.GetData(),
	}
}

func (b *brain) setGoal() *Result {
	newDestination := b.CurrentConfig.locationGoal
	b.transportation.setDestination(newDestination)

	return &Result{
		log:           "Next destination set.",
		isComplete:    true,
		transportData: b.transportation.GetData(),
	}
}

func (b *brain) moveToGoal() *Result {
	for !b.transportation.currentGoalAchieved {
		b.transportation.move(timePerMoveUpdate)
	}
	return nil
}

func (b *brain) configureBrick() *Result {
	return nil
}

func (b *brain) returnToBase() *Result {
	return nil
}
