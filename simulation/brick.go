package simulation

import (
	"leolaunches.com/assembly/physics"
)

const (
	// Sides
	front   sideLocation = "front"
	top     sideLocation = "top"
	right   sideLocation = "right"
	back    sideLocation = "back"
	bottom  sideLocation = "bottom"
	left    sideLocation = "left"
	invalid sideLocation = "invalid"

	// Dimensions
	defaultLength = 1
	defaultDepth  = 0.25
	defaultHeight = 1
	numberOfSides = 6

	// Connection types
	electric   connType = "electric"
	insulation connType = "insulation"
	chemical   connType = "chemical"
)

type connType string
type sideLocation string

var sides = []sideLocation{front, top, right, back, bottom, left}
var maleSides = map[sideLocation]bool{"back": true, "left": true, "bottom": true}
var femaleSides = map[sideLocation]bool{"front": true, "right": true, "top": true}

type brick struct {
	// Configuration Data
	id         string                      `json:"id"`
	connectors map[sideLocation]*connector `json:"connectors"`
	// Connections  map[SideLocation]map[ConnType][]string `json:"connections"`
	locationGoal *physics.Vector `json:"location_goal"`

	// DroneConnector

	// physical attributes
	height             float64
	length             float64
	depth              float64
	*physics.Kinematic `json:"kinematic"`
}

type connector struct {
	openConnections map[connType]map[sideLocation]bool
	connectedTo     *connector
	parentBrick     *brick
	brickID         string
	side            sideLocation
	angleConnected  float64
	isLocked        bool
	isFemale        bool
	isExtended      bool
}

func newConnector(side sideLocation, b *brick) *connector {
	isMale, doesExist := maleSides[side]
	isFemale := !doesExist || !isMale

	return &connector{
		openConnections: map[connType]map[sideLocation]bool{},
		side:            side,
		brickID:         b.id,
		parentBrick:     b,
		isFemale:        isFemale,
	}
}

func newBrick(id string) *brick {
	b := &brick{
		id:     id,
		height: defaultHeight,
		length: defaultLength,
		depth:  defaultDepth,
	}
	for i := range sides {
		side := sides[i]
		b.connectors[side] = newConnector(side, b)
	}
	return b
}

func (b *brick) setDims(id string, length, depth float64) {
	b.height = length
	b.length = length
	b.depth = depth
}

func (b *brick) uploadConfiguration(config *brick) {
	// Go through config and set all lock and connections accordingly.
	b.id = config.id
	for i := range sides {
		side := sides[i]
		b.connectors[side].openConnections = config.connectors[side].openConnections
	}

}

func (c *connector) connectBrick(conn *connector) {
	c.connectedTo = conn
	conn.connectedTo = c

	conn.isExtended = c.isFemale && !conn.isFemale
	c.isExtended = !c.isFemale && conn.isFemale

	if c.isExtended || conn.isExtended {
		conn.isLocked = true
		c.isLocked = true
	}
}
