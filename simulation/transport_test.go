package simulation

import (
	"testing"

	"leolaunches.com/assembly/physics"
)

func TestWillReachGoalNextTime(t *testing.T) {
	currentLocation := physics.NewVector(0, 0, 0)
	goalLocation := physics.NewVector(1, 0, 0)
	nextLocation := physics.NewVector(2, 0, 0)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(3, 0, 0)
	nextLocation = physics.NewVector(2, 0, 0)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(2, 0, 0)
	goalLocation = physics.NewVector(1, 0, 0)
	nextLocation = physics.NewVector(0, 0, 0)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(2, 0, 0)
	goalLocation = physics.NewVector(-1, 0, 0)
	nextLocation = physics.NewVector(0, 0, 0)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 1, 0)
	nextLocation = physics.NewVector(0, 2, 0)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 3, 0)
	nextLocation = physics.NewVector(0, 2, 0)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(0, 2, 0)
	goalLocation = physics.NewVector(0, 1, 0)
	nextLocation = physics.NewVector(0, 0, 0)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 2, 0)
	goalLocation = physics.NewVector(0, -1, 0)
	nextLocation = physics.NewVector(0, 0, 0)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 0, 1)
	nextLocation = physics.NewVector(0, 0, 2)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 0, 3)
	nextLocation = physics.NewVector(0, 0, 2)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(0, 0, 2)
	goalLocation = physics.NewVector(0, 0, 1)
	nextLocation = physics.NewVector(0, 0, 0)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 0, 2)
	goalLocation = physics.NewVector(0, 0, -1)
	nextLocation = physics.NewVector(0, 0, 0)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 2, 3)
	nextLocation = physics.NewVector(3, 3, 3)

	if !willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should reach goal next time but does not.")
	}

	currentLocation = physics.NewVector(0, 0, 0)
	goalLocation = physics.NewVector(0, 2, -3)
	nextLocation = physics.NewVector(3, 3, 3)

	if willReachGoalNextTime(currentLocation, nextLocation, goalLocation) {
		t.Fatal("Should not reach goal next time but does.")
	}
}
