package simulation

import (
	"encoding/json"

	"leolaunches.com/assembly/physics"
)

type transportState string

const (
	transportMass = 50.0

	standby    transportState = "waiting"
	applyForce transportState = "apply_force"
	coast      transportState = "coast"
	slowDown   transportState = "slow_down"
)

type transport struct {
	EnergyCapacity        float64 `json:"energy_capacity"` //  J
	fullThrottlePowerRate float64 // J/s
	fullThrottleForce     float64 // N

	// brain              *brain.Brain
	CurrentDestination  *physics.Vector `json:"current_destination,omitempty"`
	homeBase            *delivery
	currentGoalAchieved bool

	state transportState

	*physics.Kinematic `json:"kinematics"`
}

func newTransport() *transport {
	ts := &transport{
		CurrentDestination: physics.NewVector(0, 0, 0),
		Kinematic:          physics.NewKinematic(transportMass),
		state:              standby,
	}
	return ts
}

func (t *transport) getDelivery() *delivery {
	return t.homeBase
}

func (t *transport) returnToBase() {
	t.CurrentDestination = physics.NewVector(0, 0, 0)
	t.markGoalAchieved() //TODO move to the "move" function so it checks everytime updated.
}

// func (ts *TransportSystem) AttachBrain(brain *brain.Brain) {
// 	ts.brain = brain
// }

// Will use algorithm to determine best location eventually. For now will just do straight line.
func (t *transport) setDestination(destination *physics.Vector) {
	t.CurrentDestination = destination
	t.currentGoalAchieved = false
	// t.markGoalAchieved() //TODO move to the "move" function so it checks everytime updated.
	// ts.brain.MarkGoalAchieved(ts.currentDestination)
}

func (t *transport) markGoalAchieved() {
	t.currentGoalAchieved = true
}

func (t *transport) requestLocation() {

}

func (t *transport) move(dt float64) {
	switch t.state {
	case standby:
		t.state = applyForce
		direction := t.CurrentDestination.Subtract(t.Location).Normalize()
		t.SetForce(direction.MultiplyScalar(t.Mass))
		t.Update(dt)
		t.state = coast
		break
	case coast:
		t.SetForce(physics.NewVector(0, 0, 0))
		t.Update(dt)
		nextLocation := t.Velocity.MultiplyScalar(dt).Add(t.Location)
		if willReachGoalNextTime(t.Location, nextLocation, t.CurrentDestination) {
			t.state = slowDown
		}
		break
	case slowDown:

	}
}

func willReachGoalNextTime(currentLocation, nextLocation, goalLocation *physics.Vector) bool {
	crossingX := ((nextLocation.X >= goalLocation.X && goalLocation.X >= currentLocation.X) ||
		(currentLocation.X >= goalLocation.X && goalLocation.X >= nextLocation.X))

	crossingY := ((nextLocation.Y >= goalLocation.Y && goalLocation.Y >= currentLocation.Y) ||
		(currentLocation.Y >= goalLocation.Y && goalLocation.Y >= nextLocation.Y))

	crossingZ := ((nextLocation.Z >= goalLocation.Z && goalLocation.Z >= currentLocation.Z) ||
		(currentLocation.Z >= goalLocation.Z && goalLocation.Z >= nextLocation.Z))

	return crossingX && crossingY && crossingZ
}

func (t *transport) GetData() string {
	data, err := json.Marshal(t)
	if err != nil {
		return "Error: unable to retrieve transport data."
	}

	return string(data)
}

// func (t *transport) releaseBrick() string {
// if len(brickIDs) == 0 {
// 	return ""
// }
// currBrickID := brickIDs[0]
// brickIDs = brickIDs[1:]
// return currBrickID
// }
