package simulation

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type State string

type Action func() *Result

type Mission struct {
	Environment string `json:"environment"`

	DeliveryCapacity int               `json:"delivery_capacity"`
	BricksToPlace    []*brick          `json:"bricks_queue"`
	PlacedBricks     map[string]*brick `json:"placed_bricks,omitempty"`

	Results []Result `json:"results"`

	timeRunning float64

	state State
}

type Result struct {
	isComplete    bool `json:"is_complete,omitempty"`
	time          float64
	transportData string `json:"transport"`
	log           string `json:"log"`
}

func NewMission() *Mission {
	return &Mission{
		state: waiting,
	}
}

func (m *Mission) Run() {
	// m.nextState()
}

// func (m *Mission) AddBrick(brick *brick.Brick) {
// 	m.BricksToPlace = append(m.BricksToPlace, brick)
// }

func (m *Mission) PopBrickConfiguration() *brick {
	brick := m.BricksToPlace[m.size()]
	m.BricksToPlace = m.BricksToPlace[:m.size()-1]
	return brick
}

func (m *Mission) size() int {
	return len(m.BricksToPlace)
}

func (m *Mission) AddPlacedBrick(b *brick) {
	if m.PlacedBricks == nil {
		m.PlacedBricks = make(map[string]*brick)
	}
	m.PlacedBricks[b.id] = b
}

func LoadMissionFromFile(fn string) *Mission {
	configBytes, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Fatal("Unable to read config file.")
	}

	var configuration = NewMission()

	err = json.Unmarshal(configBytes, &configuration)

	if err != nil {
		log.Fatal("Unable to read unmarshal config file.", err)
	}

	return configuration
}

func (m *Mission) String() string {
	missionJSON, err := json.Marshal(m)

	if err != nil {
		log.Fatal("Failed to convert mission to string.")
	}

	return string(missionJSON)
}
